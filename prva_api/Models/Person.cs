﻿using System;
namespace prva_api.Models
{
    public class Person
    {
        public int id { get; set; }
        public string Gender { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime BirthDate { get; set; }
        public string Country { get; set; }
    }
}
