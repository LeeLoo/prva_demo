﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Text.Encodings.Web;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using prva_api.Models;

namespace prva_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PersonController : ControllerBase
    {
        private readonly ILogger<PersonController> _logger;
        private readonly IList<Person> people;

        public PersonController(ILogger<PersonController> logger)
        {
            _logger = logger;
            var jsonString = System.IO.File.ReadAllText("../Names.json");
            people = JsonConvert.DeserializeObject<List<Person>>(jsonString);
        }

        // person?year=1978&month=4
        /// <summary>
        /// Returns persons born in selected year and month
        /// </summary>
        /// <param name="year">selected year</param>
        /// <param name="month">selected month</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Get(int year, int month)
        {
            return Ok(people.Where(person => (person.BirthDate.Year == year) &&
                                             (person.BirthDate.Month == month)));
        }
    }
}
