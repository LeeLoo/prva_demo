
### Priprava podatkov

1. kreirati direktorij in vanj kopirati vse datoteke iz mape "excel"

2. pripraviti virtualno okolje 
    ```bash
    python3 -m venv venv
    ```
3. aktivirati virtualno okolje 
    ```bash
    source venv/bin/activate
    ```
4. namestiti potrebne package
    ```bash
    pip install -r requirements.txt
    ```
5. zagnati jupyter notebook
    ```bash
    jupyter notebook
    ```
6. odpreti "Read_xlsx_Write_Json.ipynb"

### Kreiranje baze 

SQL stavki se nahajajo v datoteki "creating DB.txt"

### Demo app, Swagger, Docker
Za zagon aplikacije v Docker containerju je priložen Dockerfile.

Najprej moramo zgraditi Docker image z ukazom
```bash
docker build -t prva_image .
```

Če želimo aplikacijo pognati v Docker containerju, je potrebno v spodnji komandni vrstici zamenjati pot do certifikata strežnika.
V spodnjem primeru je uporabljena pot, kjer ima developer shranjen svoj dev-cert.

```bash
docker run --rm -it -p 80:80 -p 443:443 \
-e ASPNETCORE_URLS="https://+;http://+" \
-e ASPNETCORE_HTTPS_PORT=5001 \
-e ASPNETCORE_Kestrel__Certificates__Default__Password="klobasa" \
-e ASPNETCORE_Kestrel__Certificates__Default__Path=/https/aspnetapp.pfx \
-v /Users/developer/.aspnet/https:/https/ prva_docker
```

Aplikacija je potem dosegljiva na https://localhost/home

Swagger dosegljiv na https://localhost/swaggger